import {AbstractModelService} from 'services/AbstractModelService';
import {Model} from 'mongoose';
import {SupplierPart, SupplierPartModel} from 'models/SupplierPart';

export class SupplierPartService extends AbstractModelService {
    get model(): Model<SupplierPart> {
        return SupplierPartModel;
    }

    get collectionName(): string {
        return 'supplierParts';
    }
}

export let supplierPartService: SupplierPartService = new SupplierPartService();
