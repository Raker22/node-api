export class ErrorResponse {
    // a simple custom error to set name and message
    // returned errors will be guaranteed to have these properties
    constructor(public message: string, public name: string = 'Error') {

    }
}
