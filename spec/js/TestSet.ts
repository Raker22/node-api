export class TestSet {
    constructor(public tests: string[], public baseUrl: string = '') {
        if (this.baseUrl.length > 0 && this.baseUrl[this.baseUrl.length - 1] !== '/') {
            // ensure the base url ends with a / if it is not empty
            this.baseUrl = `${this.baseUrl}/`;
        }
    }

    getTests(): string[] {
        return this.tests.map((test: string) => {
            return `${this.baseUrl}${test}`;
        });
    }
}
