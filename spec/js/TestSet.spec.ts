import * as chai from 'chai';
import {TestSet} from './TestSet';

chai.should();

describe('TestSet', () => {
    let tests: string[];

    beforeEach(() => {
        tests = ['test1', 'test2'];
    });

    describe('constructor', () => {
        it('returns a TestSet with default parameters', () => {
            let testSet: TestSet = new TestSet(tests);

            testSet.tests.should.equal(tests);
        });

        it('returns a TestSet with the given parameters', () => {
            let expectedBaseUrl: string = 'url/';
            let testSet: TestSet = new TestSet(tests, expectedBaseUrl);

            testSet.tests.should.equal(tests);
            testSet.baseUrl.should.equal(expectedBaseUrl);
        });

        it('adds \'/\' to the end of base url if it is not empty', () => {
            let baseUrl: string = 'url';
            let testSet: TestSet = new TestSet(tests, baseUrl);

            testSet.tests.should.equal(tests);
            testSet.baseUrl.should.equal(`${baseUrl}/`);
        });
    });

    describe('getTests', () => {
        it('returns the list of tests to run', () => {
            let baseUrl: string = 'url/';
            let testSet: TestSet = new TestSet(tests, baseUrl);
            let expectedTests = tests.map((test: string) => {
                return `${baseUrl}${test}`;
            });

            testSet.getTests().should.deep.equal(expectedTests);
        });
    });
});

