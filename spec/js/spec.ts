import '../../src/js/env';
import {TestSet} from './TestSet';
import {config} from 'Config';
import * as mongoose from 'mongoose';

const testBaseUrl: string = '../../spec/js/'; // the relative path to the test directory from tsconfig baseUrl
const testExtension: string = '.spec'; // the extension that test files have (before .ts)

// add a TestSet for each set (directory) of tests that we're testing
const testSets: TestSet[] = [
    new TestSet([
        'TestSet'
    ]),
    new TestSet([
        'QueryUtils'
    ], 'utils'),
    new TestSet([
        'ErrorService',
        'AbstractModelService'
    ], 'services')
];

before(() => {
    // connect to test database before running tests
    config.dbName = 'test';
    return mongoose.connect(config.dbUrl, config.connectionOptions).then();
});

after(() => {
    // drop the database after running all of the tests
    return mongoose.connection.db.dropDatabase().then(() => {
        return mongoose.connection.close();
    });
});

// run all of the test sets after connecting to the test db
testSets.forEach((testSet: TestSet) => {
    // run all of the tests in the test set
    testSet.getTests().forEach((test: string) => {
        require(`${testBaseUrl}${test}${testExtension}`);
    });
});
