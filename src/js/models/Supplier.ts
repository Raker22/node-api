import * as mongoose from 'mongoose';
import {Model, Schema, Document} from 'mongoose';

export interface Supplier extends Document {
    name: string;
}

export let supplier: Schema = new Schema({
    name: {
        type: String,
        required: true
    }
});

export let SupplierModel: Model<Supplier> = mongoose.model('Supplier', supplier);
