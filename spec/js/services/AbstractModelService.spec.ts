import * as chai from 'chai';
import * as sinon from 'sinon';
import {SinonStub} from 'sinon';
import * as sinon_chai from 'sinon-chai';
import {Document, Model} from 'mongoose';
import {AbstractModelService} from 'services/AbstractModelService';
import {TestDocument, TestDocumentModel} from './../TestDocument';
import {queryUtils} from 'utils/QueryUtils';

chai.use(sinon_chai);
chai.should();

class AbstractModelServiceImpl extends AbstractModelService {
    get model(): Model<any> {
        return TestDocumentModel;
    }

    get collectionName(): string {
        return 'testDocuments';
    }
}

describe('AbstractModelService', () => {
    let modelService: AbstractModelService;

    beforeEach(() => {
        modelService = new AbstractModelServiceImpl();
    });

    describe('findOne', () => {
        let includeStub: SinonStub;
        let doc: Document;

        before(() => {
            includeStub = sinon.stub(queryUtils, 'include');

            doc = new TestDocumentModel({
                name: 'random string',
                number: 21
            });

            return doc.save();
        });

        after(() => {
            console.log('Stub restored');
            includeStub.restore();

            return TestDocumentModel.remove({}).exec();
        });

        it('returns a query for the document with the given id', () => {
            let includes = [
                'include1',
                'include2',
                'include3'
            ];

            return modelService.findOne(doc.id, includes).then((result: Document) => {
                includeStub.should.have.been.calledOnce;
                includeStub.args[0][1].should.equal(includes);
                result.toObject().should.deep.equal(doc.toObject());
            });
        });
    });

    describe('findMany', () => {
        let includeStub: SinonStub;
        let docs: Document[];

        before(() => {
            includeStub = sinon.stub(queryUtils, 'include');

            return TestDocumentModel.insertMany([
                new TestDocumentModel({
                    name: 'random string 1',
                    number: 21
                }),
                new TestDocumentModel({
                    name: 'random string 2',
                    number: 514
                }),
                new TestDocumentModel({
                    name: 'random string 3',
                    number: 2
                })
            ]).then((result: Document[]) => {
                docs = result;
            });
        });

        after(() => {
            includeStub.restore();

            return TestDocumentModel.remove({});
        });

        it('returns a query for the documents matching the given query parameters', () => {
            let includes = [
                'include1',
                'include2',
                'include3'
            ];

            return modelService.findMany({number: { $lte: 21 } }, includes).then((results: Document[]) => {
                let docObjs: object[] = results.map((doc) => {
                    return doc.toObject();
                });

                includeStub.should.have.been.calledOnce;
                includeStub.args[0][1].should.equal(includes);
                docObjs.should.deep.include(docs[0].toObject());
                docObjs.should.not.deep.include(docs[1].toObject());
                docObjs.should.deep.include(docs[2].toObject());
            });
        });
    });

    describe('create', () => {
        let docData: object;
        let doc: Document;

        before(() => {
            docData = {
                name: 'test doc',
                number: 8
            };

            doc = new TestDocumentModel(docData);
        });

        after(() => {
            return TestDocumentModel.remove({});
        });

        it('adds a new document to the collection', () => {
            return modelService.create(docData).then((result: Document) => {
                doc = result;

                return TestDocumentModel.find().then((results: Document[]) => {
                    results.length.should.equal(1);
                    results[0].toObject().should.deep.equals(doc.toObject());
                });
            });
        });
    });

    describe('update', () => {
        let doc: TestDocument;

        before(() => {
            doc = new TestDocumentModel({
                name: 'random string',
                number: 21
            });

            return doc.save();
        });

        after(() => {
            return TestDocumentModel.remove({});
        });

        it('updates the existing document with the given id', () => {
            let expectedNumber: number = 25;

            return modelService.update(doc.id, { number: expectedNumber }).then((result: Document) => {
                doc.number = expectedNumber;
                result.toObject().should.deep.equal(doc.toObject());

                return TestDocumentModel.findById(doc.id).then((result: Document) => {
                    result.toObject().should.deep.equal(doc.toObject());
                });
            });
        });
    });

    describe('delete', () => {
        let doc: Document;

        before(() => {
            doc = new TestDocumentModel({
                name: 'random string',
                number: 21
            });

            return doc.save();
        });

        after(() => {
            return TestDocumentModel.remove({});
        });

        it('deletes the existing document with the given id', () => {
            return modelService.delete(doc.id).then((result: Document) => {
                result.toObject().should.deep.equal(doc.toObject());

                return TestDocumentModel.findById(doc.id).then((result: Document) => {
                    (result == null).should.equal(true);
                });
            });
        });
    });
});
