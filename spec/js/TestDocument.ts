import * as mongoose from 'mongoose';
import {Model, Schema, Document} from 'mongoose';

export interface TestDocument extends Document {
    name: string;
    number: number;
}

export let testDocument: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    number: {
        type: Number,
        required: true
    }
});

export let TestDocumentModel: Model<TestDocument> = mongoose.model('TestDocument', testDocument);
