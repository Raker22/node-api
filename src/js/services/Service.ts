import {Application} from 'express';

export interface Service {
    // initialize the application for the service
    init(app: Application): void;
}
