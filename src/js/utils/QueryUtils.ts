import {Query} from 'mongoose';

export class QueryUtils {
    parse(strOrObj: string | any): any {
        // if it's a string parse it otherwise just use it
        return typeof strOrObj === 'string' ? JSON.parse(strOrObj) : strOrObj;
    }

    include(query: Query<any>, refs: any[]) {
        // populate all of the fields in refs
        if (refs != null) {
            // only run if refs are defined
            refs.forEach((ref: string) => {
                query.populate(ref);
            });
        }
    }
}

export let queryUtils: QueryUtils = new QueryUtils();
