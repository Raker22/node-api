import * as mongoose from 'mongoose';
import {Model, Schema, Document} from 'mongoose';

export interface SupplierPart extends Document {
    quantity: number;
}

export let supplierPart: Schema = new Schema({
    quantity: {
        type: Number,
        required: true,
        get: (value: number): number => {
            // force to an integer
            return Math.trunc(value);
        },
        set: (value: number): number => {
            return Math.trunc(value);
        }
    },
    supplier: {
        type: Schema.Types.ObjectId,
        ref: 'Test',
        required: true,
        index: true
    },
    part: {
        type: Schema.Types.ObjectId,
        ref: 'Part',
        required: true,
        index: true
    }
});

supplierPart.index({ supplier: 1, part: 1 }, { unique: true });

export let SupplierPartModel: Model<SupplierPart> = mongoose.model('SupplierPart', supplierPart);
