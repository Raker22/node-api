import {AbstractModelService} from 'services/AbstractModelService';
import {Model} from 'mongoose';
import {Part, PartModel} from 'models/Part';

export class PartService extends AbstractModelService {
    get model(): Model<Part> {
        return PartModel;
    }

    get collectionName(): string {
        return 'parts';
    }
}

export let partService: PartService = new PartService();
