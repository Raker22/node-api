export class Config {
    dbHost: string = 'db';
    dbPort: number = 27017;
    dbName: string = 'nodeApi';
    dbUsername: string = 'nodeuser';
    dbPassword: string = 'inodeunode';

    hostPort: number = 3000;
    externalPort: number = 8080;
    hostName: string = 'http://localhost';

    get url(): string {
        return `${this.hostName}:${this.externalPort}`;
    }

    get dbUrl(): string {
        // need to include username, password, and authSource in order to connect
        return `mongodb://${this.dbUsername}:${this.dbPassword}@${this.dbHost}:${this.dbPort}/${this.dbName}?authSource=admin`;
    }

    get connectionOptions(): object {
        // need to set the authorization database
        return {
            useMongoClient: true
        };
    }
}

export let config: Config = new Config();
