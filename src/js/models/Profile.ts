import * as mongoose from 'mongoose';
import {Model, Schema, Document} from 'mongoose';
import {Profile} from 'passport';

export interface UserProfile extends Document, Profile {
    provider: string;
    id: string;
    displayName: string;
    name: {
        familyName: string;
        givenName: string;
        middleName: string;
    };
    emails: {
        value: string;
        type: string;
    }[];
    photos: {
        value: string
    }[];
}

export let userProfile: Schema = new Schema({
    provider: {
        type: String,
        required: true
    },
    id: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    displayName: {
        type: String,
        required: true
    },
    name: {
        type: {
            familyName: {
                type: String,
                required: true
            },
            givenName: {
                type: String,
                required: true
            },
            middleName: {
                type: String
            }
        },
        required: true
    },
    emails: [{
        value: {
            type: String,
            required: true
        },
        type: {
            type: String
        }
    }],
    photos: [{
        value: {
            type: String,
            required: true
        }
    }]
});

export let FacebookProfileModel: Model<UserProfile> = mongoose.model('FacebookProfile', userProfile);
export let GoogleProfileModel: Model<UserProfile> = mongoose.model('GoogleProfile', userProfile);
