// set base dir to wherever service.js is running from
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();

// use default promises with mongoose instead of mpromise (deprecated)
require('mongoose').Promise = Promise;
