import {AbstractModelService} from 'services/AbstractModelService';
import {Model} from 'mongoose';
import {Supplier, SupplierModel} from 'models/Supplier';

export class SupplierService extends AbstractModelService {
    get model(): Model<Supplier> {
        return SupplierModel;
    }

    get collectionName(): string {
        return 'suppliers';
    }
}

export let supplierService: SupplierService = new SupplierService();
