import './env'; // updates environment variables to allow importing using tsconfig baseUrl
import * as express from 'express';
import {Application} from 'express';
import {Service} from 'services/Service';
import {supplierService} from 'services/SupplierService';
import {partService} from 'services/PartService';
import {supplierPartService} from 'services/SupplierPartService';
import {appService} from 'services/AppService';
import {config} from 'Config';
import * as mongoose from 'mongoose';
import {middlewareService} from 'services/MiddlewareService';
import {errorService} from 'services/ErrorService';
import {loginService} from 'services/LoginService';

export const app: Application = express();
const services: Service[] = [ // services to be initialized
    middlewareService,
    loginService,
    supplierService,
    partService,
    supplierPartService,
    appService,
    errorService
];

mongoose.connect(config.dbUrl, config.connectionOptions).then(() => {
    // once we establish a connection to the database start the app
    services.forEach((service: Service) => {
        // initialize each service
        service.init(app);
    });

    app.listen(config.hostPort, (error: any) => {
        if (error != null) {
            throw error;
        }
        else {
            console.log(`Server listening on ${config.hostPort}`);
        }
    });
})
.catch((error: any) => {
    errorService.handleError(error);
});
