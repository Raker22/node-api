import {queryUtils} from 'utils/QueryUtils';
import {Application, NextFunction, Request, Response} from 'express';
import {ModelService} from 'services/ModelService';
import {Document, Model, Query} from 'mongoose';
import {ErrorResponse} from 'ErrorResponse';

export abstract class AbstractModelService implements ModelService {
    static nameToUrl(collectionName: string): string {
        return collectionName.toLowerCase();
    }

    abstract model: Model<any>;
    abstract collectionName: string;

    get collectionUrl(): string {
        return AbstractModelService.nameToUrl(this.collectionName);
    }

    findOne(id: string, include: any = null): Query<Document> {
        // get the document with the given id
        let query: Query<any> = this.model.findById(id);

        // include refs
        queryUtils.include(query, include);

        return query;
    }

    findMany(queryParams: any, include: any = null): Query<Document[]> {
        // get all of the documents in the model matching the query
        let query: Query<any> = this.model.find(queryParams);

        // include refs
        queryUtils.include(query, include);

        return query;
    }

    create(data: object): Promise<Document> {
        // create a new document and return it
        let doc: Document = new this.model(data);

        return doc.save();
    }

    update(id: string, data: object): Query<Document> {
        // update the document with the given id and return it
        return this.model.findByIdAndUpdate(id, data, { new: true });
    }

    delete(id: string): Query<Document> {
        // delete the document with the given id and return it
        return this.model.findByIdAndRemove(id);
    }

    getOneInit(app: Application) {
        app.get(`/${this.collectionUrl}/:id`, (request: Request, response: Response, next: NextFunction) => {
            // get the document with the given id
            let include: any = queryUtils.parse(request.query.include);

            this.findOne(request.params.id, include).lean().then((doc: any) => {
                this.handleExists(doc, request.params.id, response);
            })
            .catch((error: any) => {
                next(error);
            });
        });
    }

    getManyInit(app: Application) {
        app.get(`/${this.collectionUrl}`, (request: Request, response: Response, next: NextFunction) => {
            // get all of the documents in the model matching the query
            let queryParams: any = queryUtils.parse(request.query.query);
            let include: any = queryUtils.parse(request.query.include);

            this.findMany(queryParams, include).lean().then((docs: any) => {
                response.send(docs);
            })
            .catch((error: any) => {
                next(error);
            });
        });
    }

    postInit(app: Application) {
        app.post(`/${this.collectionUrl}`, (request: Request, response: Response, next: NextFunction) => {
            // create a new document and return it
            this.create(request.body).then((doc: Document) => {
                response.send(doc);
            })
            .catch((error: any) => {
                next(error);
            });
        });
    }

    putInit(app: Application) {
        // acts like patch but that's probably more convenient anyway
        app.put(`/${this.collectionUrl}/:id`, (request: Request, response: Response, next: NextFunction) => {
            // update the document with the given id and return it
            this.update(request.params.id, request.body).lean().then((doc: Document) => {
                this.handleExists(doc, request.params.id, response);
            })
            .catch((error: any) => {
                next(error);
            });
        });
    }

    deleteInit(app: Application): void {
        app.delete(`/${this.collectionUrl}/:id`, (request: Request, response: Response, next: NextFunction) => {
            // delete the document with the given id and return it
            this.delete(request.params.id).lean().then((doc: Document) => {
                // the result on the database is the same whether the doc exists or not but
                // the user might be expecting data back if this succeeds so error if it doesn't
                this.handleExists(doc, request.params.id, response);
            })
            .catch((error: any) => {
                next(error);
            });
        });
    }


    handleExists(doc: Document, id: any, response: Response): void {
        if (doc == null) {
            // if the result is null then the doc doesn't exist
            response.status(404).send(new ErrorResponse(`${this.model.modelName} ${id} does not exist`, 'NotFoundError'));
        }
        else {
            // otherwise return the doc
            response.send(doc);
        }
    }

    init(app: Application) {
        // initialize app
        this.getOneInit(app);
        this.getManyInit(app);
        this.postInit(app);
        this.putInit(app);
        this.deleteInit(app);
    }
}
