import {Service} from 'services/Service';
import {Application, Response} from 'express';
import {Model, Document, Query} from 'mongoose';

export interface ModelService extends Service {
    collectionName: string; // the name of the corresponding collection
    collectionUrl: string; // the url to access the collection
    model: Model<any>; // the model that corresponds to this service

    // service methods
    findOne(id: string, include: any): Query<Document>;
    findMany(queryParams: any, include: any): Query<Document[]>;
    create(data: object): Promise<Document>;
    update(id: string, data: object): Query<Document>;
    delete(id: string): Query<Document>;

    // application initialization methods
    getOneInit(app: Application): void; // initialize route to get one document
    getManyInit(app: Application): void; // initialize route to get many documents
    postInit(app: Application): void; // initialize route to create a document
    putInit(app: Application): void; // initialize route to update a document
    deleteInit(app: Application): void; // initialize route to delete a document

    handleExists(doc: Document, id: any, response: Response): void; // respond based on if the document exists

    init(app: Application): void;
}
