import * as chai from 'chai';
import * as sinon from 'sinon';
import {SinonStub} from 'sinon';
import * as sinon_chai from 'sinon-chai';
import * as httpMock from 'node-mocks-http';
import {Request, Response} from 'express';
import {ErrorResponse} from 'ErrorResponse';
import {errorService} from 'services/ErrorService';

chai.use(sinon_chai);
chai.should();

describe('errorService', () => {
    describe('handleError', () => {
        it('logs the error', () => {
            let expectedError: Error = new Error('Oh no!');
            let errorStub: SinonStub = sinon.stub(console, 'error');

            errorService.handleError(expectedError);

            console.error.should.have.been.calledOnce.calledWith(expectedError);

            errorStub.restore();
        });
    });

    describe('handleRequestError', () => {
        let request: Request;
        let response: Response;

        let handleErrorStub: SinonStub;
        let sendStub: SinonStub;

        beforeEach(() => {
            request = httpMock.createRequest();
            response = httpMock.createResponse();

            handleErrorStub = sinon.stub(errorService, 'handleError');
            sendStub = sinon.stub(response, 'send');
            sinon.spy(response, 'status');
        });

        afterEach(() => {
            handleErrorStub.restore();
        });

        it('sends a 500 response with the error name and message as the body', () => {
            let expectedResponse: ErrorResponse = new ErrorResponse('oh no', 'random error');

            errorService.handleRequestError(expectedResponse, request, response);

            errorService.handleError.should.have.been.calledOnce.calledWith(expectedResponse);
            response.status.should.have.been.calledOnce.calledWith(500);
            response.send.should.have.been.calledOnce.calledWith(expectedResponse);
        });

        it('it sends a 500 response with the error as the message if it is a string', () => {
            let error: string = 'oh no';

            errorService.handleRequestError(error, request, response);

            errorService.handleError.should.have.been.calledOnce.calledWith(error);
            response.status.should.have.been.calledOnce.calledWith(500);
            response.send.should.have.been.calledOnce;
            sendStub.args[0][0].should.have.property('name').that.is.a('string');
            sendStub.args[0][0].should.have.property('message', error);
        });

        it('it sends a 500 response with a generic error if the error is not an error or string', () => {
            let error: object = {};

            errorService.handleRequestError(error, request, response);

            errorService.handleError.should.have.been.calledOnce.calledWith(error);
            response.status.should.have.been.calledOnce.calledWith(500);
            response.send.should.have.been.calledOnce;
            sendStub.args[0][0].should.have.property('name').that.is.a('string');
            sendStub.args[0][0].should.have.property('message').that.is.a('string');
        });
    });
});
