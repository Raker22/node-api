import {Service} from 'services/Service';
import {Application, Request, Response, NextFunction} from 'express';
import {ErrorResponse} from 'ErrorResponse';

export class ErrorService implements Service {
    handleError(error: any) {
        // default error handler
        console.error(error);
    }

    handleRequestError(error: any, request: Request, response: Response) {
        // error handler for requests
        // doesn't do anything with request right now but we might want it to later
        this.handleError(error);

        let result: ErrorResponse;

        if (error.hasOwnProperty('name') && error.hasOwnProperty('message')) {
            // if error is an Error return it
            result = new ErrorResponse(error.message, error.name);
        }
        else {
            // use the error as the message if it's a string otherwise use a default message
            result = new ErrorResponse(typeof error === 'string' ? error : 'An error occurred');
        }

        response.status(500).send(result);
    }

    init(app: Application): void {
        // error handler (has to have next function param or it won't work right)
        app.use((error: any, request: Request, response: Response, next: NextFunction) => {
            this.handleRequestError(error, request, response);
            next(error);
        });
    }
}

export let errorService: ErrorService = new ErrorService();
