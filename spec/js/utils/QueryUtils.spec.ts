import * as chai from 'chai';
import {queryUtils} from 'utils/QueryUtils';
import {SupplierModel} from 'models/Supplier';
import {Query} from 'mongoose';
import {SinonStub} from 'sinon';
import * as sinon from 'sinon';
import * as sinon_chai from 'sinon-chai';

chai.use(sinon_chai);
chai.should();

describe('dbUtils', () => {
    describe('parse', () => {
        let expectedObject: any;

        beforeEach(() => {
            expectedObject = [
                { obj: { val: 1 } },
                { obj: { val: 2 } }
            ];
        });

        it('returns the argument', () => {
            let actucalObject: any = queryUtils.parse(expectedObject);

            actucalObject.should.deep.equal(expectedObject);
        });

        it('parses the argument when it is a string', () => {
            let objectString: string = '[{"obj":{"val":1}},{"obj":{"val":2}}]';
            let actucalObject: any = queryUtils.parse(objectString);

            actucalObject.should.deep.equal(expectedObject);
        });
    });

    describe('include', () => {
        let query: Query<any>;
        let populateSpy: SinonStub;

        beforeEach(() => {
            query = SupplierModel.find();
            populateSpy = sinon.stub(query, 'populate');
        });

        it('populates the given fields', () => {
            let refs: string[] = [
                'include1',
                'include2',
                'include3'
            ];

            queryUtils.include(query, refs);

            populateSpy.should.have.been.calledThrice;
            refs.forEach((include: string) => {
                populateSpy.should.have.been.calledWith(include);
            });
        });

        it('only runs if refs isn\'t null', () => {
            let refs = null;

            queryUtils.include(query, refs);

            populateSpy.should.not.have.been.called;
        });
    });
});
