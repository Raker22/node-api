import {Service} from 'services/Service';
import {Application, Request, Response} from 'express';
import * as passport from 'passport';

export class AppService implements Service {
    init(app: Application): void {
        // initialize all of the endpoints not associated with collections
        app.get('/echo', (request: Request, response: Response) => {
            // echo endpoint for GET
            response.send(request.query);
        });

        app.all('/echo', (request: Request, response: Response) => {
            // echo endpoint for all other request types
            response.send(request.body);
        });

        app.get('/objectId', (request: Request, response: Response) => {
            response.send(request.query.query);
        });

        app.all('/*', (request: Request, response: Response) => {
            // if the request made it this far then the resource is not found
            response.status(404).send();
        });
    }
}

export let appService = new AppService();
