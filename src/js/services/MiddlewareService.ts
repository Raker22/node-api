import {Service} from 'services/Service';
import {Application} from 'express';
import * as helmet from 'helmet';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as passport from 'passport';
import {Strategy as FacebookStrategy} from 'passport-facebook';
import {config} from 'Config';

export class MiddlewareService implements Service {
    init(app: Application): void {
        // add middleware to application
        app.use(helmet());
        app.use(bodyParser.json());
        app.use(cors());
    }
}

export let middlewareService: MiddlewareService = new MiddlewareService();
