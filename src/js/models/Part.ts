import * as mongoose from 'mongoose';
import {Model, Schema, Document} from 'mongoose';

export interface Part extends Document {
    name: string;
}

export let partSchema: Schema = new Schema({
    name: {
        type: String,
        required: true
    }
});

export let PartModel: Model<Part> = mongoose.model('Part', partSchema);
