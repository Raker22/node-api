import {Service} from 'services/Service';
import {Application, Request, Response} from 'express';
import * as passport from 'passport';
import {Profile} from 'passport';
import {Strategy as FacebookStrategy} from 'passport-facebook';
import {Strategy as GoogleStrategy} from 'passport-google-oauth2';
import {config} from 'Config';
import {FacebookProfileModel, GoogleProfileModel, UserProfile} from 'models/Profile';
import {Model} from 'mongoose';

export class LoginService implements Service {
    loginCallbackOptions: object = {
        session: false
    };

    handleLogin(model: Model<UserProfile>, request: Request, accessToken: string, refreshToken: string, profile: Profile, done: (error: any, user?: any, messages?: object) => void) {
        // get or create the user
        model.findOne({ id: profile.id }).lean().then((user: UserProfile) => {
            if (user == null) {
                // create the user if they don't exist
                return new model(profile).save().then((saved: UserProfile) => {
                    done(null, saved.toJSON());
                });
            }
            else {
                // otherwise return the existing user
                done(null, user);
            }
        })
        .catch((error: any) => {
            done(error);
        });
    }

    handleLoginCallback(request: Request, response: Response) {
        response.send(request.user);
    }

    init(app: Application): void {
        // initialize routes and middleware dealing with login
        app.use(passport.initialize());

        // facebook config
        passport.use(new FacebookStrategy(
            {
                clientID: '539806883039190',
                clientSecret: '18b65d7b8cb73decb587c76a04d874fe',
                callbackURL: `${config.url}/login/facebook/callback`,
                profileFields: ['id', 'displayName', 'emails', 'name', 'photos'],
                passReqToCallback: true
            },
            (request: Request, accessToken: string, refreshToken: string, profile: Profile, done: (error: any, user?: any, messages?: object) => void) => {
                this.handleLogin(FacebookProfileModel, request, accessToken, refreshToken, profile, done);
            }
        ));

        passport.use(new GoogleStrategy({
            clientID: '872932234435-98ukaibi64b240e1edthok72m1vqghjj.apps.googleusercontent.com',
            clientSecret: 'AWyucq2aCB-VaA-VT6iknrxo',
            callbackURL: `${config.url}/login/google/callback`,
            passReqToCallback: true
        },
        (request: Request, accessToken: string, refreshToken: string, profile: any, done: (error: any, user?: any, messages?: any) => void) => {
            this.handleLogin(GoogleProfileModel, request, accessToken, refreshToken, profile, done);
        }));

        app.get('/login/facebook', passport.authenticate('facebook'));
        app.get('/login/facebook/callback', passport.authenticate('facebook', this.loginCallbackOptions), this.handleLoginCallback);

        app.get('/login/google', passport.authenticate('google', { scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ] }));
        app.get('/login/google/callback', passport.authenticate('google', this.loginCallbackOptions), this.handleLoginCallback);
    }
}

export let loginService: LoginService = new LoginService();
